<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateToDoRequest;
use App\Http\Resources\ToDoResource;
use App\Models\ToDo;
use Illuminate\Http\Request;


class ToDoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return ToDoResource::collection(ToDo::latest()->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'task' => 'required|min:5'
        ]);

        $todo = auth()->user()->toDos()->create([
            'title' => $request->task,
        ]);

        return new ToDoResource($todo);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ToDo  $toDo
     * @return \Illuminate\Http\Response
     */
    public function edit(ToDo $toDo)
    {
        return new ToDoResource($toDo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ToDo  $toDo
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateToDoRequest $request, ToDo $toDo)
    {
        $params = $request->toArray();

        $toDo->update([
            'title' => $params['title'],
            'due_date' => $params['due_date'],
            'is_checked' => $params['is_checked'] ?? false
        ]);

        $toDo->sync($request->categories);

        return $toDo;

    }

    public function updateStatus(ToDo $toDo)
    {
        $toDo->update([
            'is_checked' => !$toDo->is_checked
        ]);

        return new ToDoResource($toDo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ToDo  $toDo
     * @return \Illuminate\Http\Response
     */
    public function destroy(ToDo $toDo)
    {
        $toDo->categories()->detach();
        return $toDo->delete();
    }
}
