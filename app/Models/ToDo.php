<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ToDo extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'due_date', 'is_checked'];

    public function categories() : BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'todo_categories', 'todo_id');
    }

    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function scopeDueForToday($query) {
        return $query->where('is_checked', false)->where('due_date', Carbon::today());
    }
}
