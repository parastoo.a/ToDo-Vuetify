<?php

namespace App\Console\Commands;


use App\Models\User;
use App\Notifications\NotifyUser as NotificationsNotifyUser;
use Carbon\Carbon;
use Illuminate\Console\Command;

class NotifyUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:notify-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will send email to users who have tasks due for that day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        User::get()->each(function($user) {
            $tasks = $user->toDos()->duefortoday()->get();
            if (count($tasks)) {
                $user->notify(new NotificationsNotifyUser($user, $tasks));
            }
        });
    }
}
