

## Installation


```
npm install

composer update

php artisan migrate --seed
```

User for login :

    . Email : parastoo@test.com

    . Password : password


Command for notification :

```
php artisan command:notify-user

```



Run jobs :

```
php artisan queue:work --queue=notify_user
```

Tasks page after login :

![Alt text](public/images/result.png?raw=true "Result")




