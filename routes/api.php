<?php

use App\Http\Controllers\CategoryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Fortify\Http\Controllers\AuthenticatedSessionController;
use App\Http\Controllers\ToDoController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


// auth
Route::middleware('auth:sanctum')->post('logout', [AuthenticatedSessionController::class, 'destroy']);
Route::post('login', [AuthenticatedSessionController::class, 'store']);

//tasks
Route::group([
    'prefix' => 'tasks',
    'middleware' => 'auth:sanctum'
], function() {
     Route::get('/', [ToDoController::class, 'index']);
     Route::post('store', [ToDoController::class, 'store']);
     Route::get('{toDo}', [ToDoController::class, 'edit']);
     Route::post('update/{toDo}', [ToDoController::class, 'update']);
     Route::post('update/status/{toDo}', [ToDoController::class, 'updateStatus']);
     Route::delete('{toDo}', [ToDoController::class, 'destroy']);
});

//categories
Route::group([
    'prefix' => 'categories',
    'middleware' => 'auth:sanctum'
], function() {
    Route::get('/', [CategoryController::class, 'index']);
});

