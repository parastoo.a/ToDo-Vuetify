<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Category>
 */
class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => ''
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function (Category $category) {
            $category->title = 'Category ' . $category->id;
            $category->save();
        });
    }
}
