<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Http\Controllers\CategoryController;
use App\Models\Category;
use App\Models\ToDo;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $user = User::factory()->create([
            'email' => 'parastoo@test.com',
        ]);

        $categories = Category::factory(10)->create();

        $toDos = ToDo::factory(50)->for($user)->create();

        $toDos->each(function($todo) use ($categories) {

            $todo->categories()->attach($categories->random(rand(0,3))->pluck('id')->toArray());

        });


    }
}
