require('./bootstrap');

import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import App from './App.vue';
import router from './router';

Vue.use(Vuetify);

const app = new Vue({
    router,
    render: h => h(App),
    vuetify: new Vuetify(),
    el:'#app',
});

