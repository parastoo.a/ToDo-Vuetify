import VueRouter from "vue-router";


import Login from "../components/LoginComponent";
import Tasks from "../components/TasksComponent";
import Vue from "vue";

const routes = [
    {
        path: "/",
        name: "Main",
    },
    {
        path: "/login",
        name: "Login",
        component: Login,
        meta:{requiresGuest: true}
    },
    {
        path: "/tasks",
        name: "Tasks",
        component: Tasks,
        meta: { requiresAuth: true },
    },
];

Vue.use(VueRouter);

const router = new VueRouter({
    routes,
    mode: 'history',
});

router.beforeEach((to, from, next) => {
    const authenticated = localStorage.getItem('authenticated');

    if(to.meta.requiresGuest && authenticated)  next('/tasks');

    else if(to.meta.requiresAuth && !authenticated) next('login');

    else next(true);


});


export default router;
