<body>
    Dear {{ $user->name}},
    <p>Please check your tasks based on their due date :</p>

    <ul>
        @foreach ($tasks as $task)
            <li>{{ $task->title}}</li>
        @endforeach

    </ul>

</body>
