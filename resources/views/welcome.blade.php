<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravel</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@6.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="{{ asset('css/app.css')}}">
</head>
<body>
    <div id="app">
        {{-- <v-app>
            <v-main>
                <v-container>
                    <router-link to="/login">Go to Home</router-link>
                    <router-link to="/tasks">Go to About</router-link>
                </v-container>
            </v-main>
        </v-app> --}}
    </div>
    <script src="{{ asset('js/app.js')}}"></script>

</body>
</html>
